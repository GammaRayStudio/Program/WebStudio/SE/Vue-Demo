import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import ElementPlus from "../views/ElementPlus.vue";
import ElementTable from "../views/ElementTable.vue";
import AdvancedTable from "../views/AdvancedTable.vue";
import AxiosAjax from "../views/AxiosAjax.vue"
import jQueryDataTable from "../views/jQueryDataTable.vue"
import jQureyDataTable2 from "../views/jQueryDataTable02.vue"
import jQueryAjax from "../views/jQueryAjax.vue"

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/element-plus",
    name: "Element Plus",
    component: ElementPlus,
  },
  {
    path: "/element-table",
    name: "Element Table",
    component: ElementTable,
  },
  {
    path: "/advanced-table",
    name: "Advanced Table",
    component: AdvancedTable,
  },
  {
    path: "/axios-ajax",
    name: "Axios Ajax",
    component: AxiosAjax,
  },
  {
    path: "/jquery-datatable",
    name: "jQuery DataTable",
    component: jQueryDataTable
  },
  {
    path : "/jquery-datatable-2",
    name: "jQuery DataTable 02",
    component: jQureyDataTable2
  },
  {
    path : "/jquery-ajax",
    name: "jQuery Ajax",
    component: jQueryAjax
  }
,
{

}];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
