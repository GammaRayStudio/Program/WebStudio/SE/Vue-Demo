from flask import Flask, render_template, request, jsonify, json
from flask_cors import CORS

app = Flask(__name__)

CORS(app)


@app.route("/data", methods=['GET'])
def data():
    if request.method == "GET":
        lstInfo = ["001", "002", "003"]
    return jsonify(lstInfo)


@app.route("/data/msg", methods=['GET'])
def dataMsg():
    if request.method == "GET":
        lstInfo = ["A", "B", "C", "D", "E"]
    return jsonify(lstInfo)


@ app.route('/data/ajax', methods=['POST'])
def locateMsg():
    # account, beacon, TX power, RSSI, timestamp
    if request.method == "POST":
        locateInfo = {
            'id': request.form['id'],
            'name': request.form['name'],
            'version': '1.0.1',
        }
        print(locateInfo)
        return jsonify(result='OK')


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
